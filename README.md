# Fetch Pick-and-place

41014 Sensing and Control for Mechatronic Systems

Major Project: Fetch Pick and Place

## Introduction

Our system uses computer vision to identify a simply colored object using blob
detection. This is then used, along with point cloud data, to get the object's
position, and this position is then transformed to be relative to the robot.
Finally, the robot creates a series of waypoints and generates a path on the
fly, with approach waypoints and adjustment for the end effector. 

## Computer Vision System

The computer vision system is performed using the open source openCV library
and is written in python. OpenCV has bindings for python and due to python's
ease of writing this was chosen.

The image from the rgb camera is used to detect the object. This is done with a
combination of a contrast mask and blob detection. This has the potential to
fail when objects of similar contrasts are in the frame, however it is fast and
effective in the absence of these things.

Once the key points in the frame have been identified this is mapped to the
point cloud and the corresponding coordinates are taken. These are taken in
relation to the camera's pose and will need to be translated to the robots
base pose.

## Reference Frame Transformation System

This node simply takes the coordinates of the pickup point relative to the eye
reference frame and transforms them to be relative to the robot base frame. This
is mostly handled by TF, the node just applies that tranform so that the pickup
coordinates are ready for the pick and place system. 

Detailed documentation of the C++ node can be found in coord_calc.cpp

## Robotic Path Planning & Control System

The robot path planning system receives a set of 3D coordinates detailing the
pickup position of the object, and uses these to generate a set of waypoint to
pick the object and place it in the position mirrored around the robot's
left-right plane. This position is given relative to the robot's base link, and
is received from the reference frame transformation system. 

First, the pick position is received and a translation and rotation are applied
to generate a suitable pose for the robot to pick the object. This pose is then
mirrored around the left-right plane of the robot, and two approach waypoints
are generated above the pick and place waypoints. This path generation is done
using the Eigen geometry library. 

Once the waypoints are generated, the robot will begin executing them. This is
done through the moveit move group interface, which simplifies planning of
joint-space and cartesian space movements. The robot uses joint moves in and out
of the approach waypoints, and follows a rectilinear cartesian path for the pick
and place, ensuring smooth motion and avoiding collisions due to the curved
paths produced by joint space movements. 

The trajectory to the next waypoint is calculated on the fly, and if at any
point the robot cannot reach the next waypoint, either through a controller
failure (e.g. collision) or planning failure (e.g. out of reach), it abandons
the trajectory and resets. 

Since the pick and place process is slow compared to the rate at which valid
object positions are published, the robot control node does not buffer the
pickup location, and will only act on one pickup location at a time. 

Gripper control is also done using the move group interface, with a separate
group for the grippers. 

Given more time, I would have liked to have adapted the node to use the extended
move group which includes the robot's prismatic joint, as this would increase
the workspace and make it easier to reach objects in front of it. 

Detailed documentation of the C++ node can be found in pick-n-place.cpp

## Additional Elements

### Launch File

Since this system consists of multiple nodes, in order to simplify startup, a
launch file is provided. This can be run to start all three elements at once. 

```
# roslaunch fetch_pick-n-place pnp.launch
```

### Head Control Node

This very simple node is used to manually adjust the camera, as there isn't
really an easy way to do this out of the box for the simulator. The usage is
simple, just run the script using rosrun and provide values for the pan and tilt
of the camera:
```
$ rosrun fetch_pick-n-place move_camera.py <pan> <tilt>
```

Detailed documentation is included in move_camera.py
