#!/usr/bin/env python

# IMPORTS
#########

# ROS module
import rospy
# Action system, built on top of ROS topics.
import actionlib
# Used to get argv for the node init
import sys
# Used to parse the arguments given to the node
import argparse
# The ROS messages needed to send the data to the head controller. 
from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectoryPoint

def __main__():
    """
    Main Thread of Execution. 

    This node is pretty bare-bones, it just initialises the ROS node, parses the
    arguments given in the command line, and then creates an extremely simple
    trajectory for the camera. This is sent to the head controller via the ROS
    action system. 
    """
    # Initialise the ROS node
    rospy.init_node("movecam", argv=sys.argv)
    # Parse the arguments for pan and tilt
    ap = argparse.ArgumentParser(description="Controls the head camera of the robot.")
    ap.add_argument("pan", type=float)
    ap.add_argument("tilt", type=float)
    args = ap.parse_args()
    rospy.loginfo("Moving to ({},{})".format(args.pan, args.tilt))
    # Use the ROS action client to control the camera
    rospy.loginfo("Connecting to the head controller action server...")
    cl = actionlib.SimpleActionClient("/head_controller/follow_joint_trajectory", FollowJointTrajectoryAction)
    cl.wait_for_server()
    g = FollowJointTrajectoryGoal()
    p1 = JointTrajectoryPoint()
    p1.positions = [args.pan, args.tilt]
    p1.velocities = [0,0]
    p1.time_from_start = rospy.Duration(secs=2, nsecs=0)
    g.trajectory.points.append(p1)
    g.trajectory.joint_names = ["head_pan_joint", "head_tilt_joint"]
    rospy.loginfo("Sending goal...")
    cl.send_goal_and_wait(g, rospy.Duration(secs=3, nsecs=0))
    rospy.loginfo("Complete!")

if __name__ == "__main__":
    __main__()
