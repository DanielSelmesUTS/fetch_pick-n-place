#!/usr/bin/env python
import rospy
import cv2
from cv_bridge import CvBridge, CvBridgeError
import time  # DEBUG: just remove this for realsises
import sys
import numpy as np
from math import sqrt

# messages
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Image, PointCloud2
from sensor_msgs import point_cloud2

# global data from the subscribers
image_data = None
point_cloud = None  # this is the base cloud object. Needs to be interpreted with the point_cloud2.read_points function

BLUE_LOWER_BOUND = (84, 98, 0)
BLUE_UPPER_BOUND = (179, 255, 255)
FOCAL_LENGTH = 55  # in metres
PRINCIPLE_POINT = (320, 240)  # in pixels

def image_recieve_rgb(data):
    """
    A subscriber to the head_camera/rgb/image_raw topic
    gets a 640x480 (w/h) image of colour data
    stores into the image_data global the opencv format image
    """

    global image_data
    image_data = CvBridge().imgmsg_to_cv2(data, "passthrough")


def point_data_recieve(data):
    """
    a subscriber for the point cloud data
    """

    global point_cloud
    point_cloud = data


def object_detect(image):
    """
    takes an image
    returns a list of blobs found in the colour blue
    """

    # creating the bask
    blur = cv2.GaussianBlur(image, (11,11), 0)
    hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, BLUE_LOWER_BOUND, BLUE_UPPER_BOUND)
    reversemask = 255 - mask

    # detecting the object
    detector = cv2.SimpleBlobDetector_create()  # detects the blobs
    keypoints = detector.detect(reversemask)  # gets the keypoints on the reversemask

    # DEBUG: displays the image for debugging
    # im = cv2.drawKeypoints(reversemask, keypoints, np.array([]), (0, 0, 255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # cv2.imshow("key", im)
    # cv2.waitKey(0)

    keypoints_list = list()
    # for each key point;
    for point in keypoints:
        keypoints_list.append(point.pt)

    return keypoints_list


def position_interpretation(key_point, cloud):
    """
    accepts the key point and point_cloud data
    uses this to find the coordinates at the key point
    returns xyz position as a Vector3
    """

    point = None

    # point_cloud2 returns a generator. it should only have one key point
    # but the for loop is still needed
    for i in point_cloud2.read_points(cloud, skip_nans=False, uvs=[key_point]):
        point = i

    return Vector3(point[0], point[1], point[2])


def main():
    """
    handles the pulling in of images and the object detection
    through to the position calculation

    once a successful position is calculated it publishes to
    the 'object_detect' topic
    """

    # creating the node
    rospy.init_node('camera_detect')
    rate = rospy.Rate(15)  # in hz, this is the refresh rate of the camera

    # SUBSCRIBERS
    rospy.Subscriber('head_camera/rgb/image_raw', Image, image_recieve_rgb)
    rospy.Subscriber('head_camera/depth_registered/points', PointCloud2, point_data_recieve)

    # PUBLISHERS
    relative_pose = rospy.Publisher('relative_pose', Vector3, queue_size=10)


    # DEBUG: this is just here for some buffering, takes a second to get any data in
    time.sleep(1)

    while not rospy.is_shutdown():
        try:
            key_point = object_detect(image_data)[0]
            key_point = (round(key_point[0]), round(key_point[1]))  # gets the integer value
            # DEBUG: outputs the key point, in x.y values
            # rospy.loginfo(depth_data[key_point[0]][key_point[1]])
        except:
            print('failed to detect any objects')
            continue

        position = position_interpretation(key_point, point_cloud)

        # outputting the data
        rospy.loginfo(position)
        relative_pose.publish(position)

        rate.sleep()



if __name__ == "__main__":
    main()
