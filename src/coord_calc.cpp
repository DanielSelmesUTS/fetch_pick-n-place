/** 
 * @file coord_calc.cpp
 * @author Daniel Selmes
 * @date 2021-05-19
 * @brief ROS C++ node which converts coordinates between two reference frames. 
 */

// INCLUDES
///////////

// ROS Runtime
#include <ros/ros.h>
// Used to get the transform from the camera frame to the robot base frame
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/Transform.h>
// Used to actually do the transform
#include <Eigen/Geometry>
// Used to convert between eigen and ros geometry representations
#include <eigen_conversions/eigen_msg.h>

// HELPER FUNCTIONS
///////////////////

/**
 * @brief Uses TF to update the tranform between two frames.
 * @param tf Transform to be updated.
 * @param tfBuf tf2 buffer object used to request the transform.
 * @return Returns true if the transform was updated (value may be unchanged).
 */
bool updateTf(geometry_msgs::Transform &tf, tf2_ros::Buffer &tfBuf) {
    
    try{
        // Attempt to update the transform
        geometry_msgs::TransformStamped eyeToBaseStamped = tfBuf.lookupTransform("base_link", "head_camera_rgb_optical_frame", ros::Time(0));
        tf = eyeToBaseStamped.transform;
        return true;
    } catch (tf2::TransformException ex) {
        // Catch an error if it occurs, and indicate that it did not update
        ROS_WARN("Failed to get base-to-eye tranform: %s", ex.what());
        return false;
    }

}

// ROS TOPIC CALLBACKS
//////////////////////

/**
 * @brief Transforms coordinates from the eye to the base reference frame. 
 * @param msg The coordinates received on the eye frame topic. 
 * @param coordPub The publisher for the base reference frame coordinates.
 * @param eyeToBase The transform from the eye reference frame to the base
 * reference frame. 
 */
void coordSubCallback(
    const geometry_msgs::Vector3ConstPtr msg,
    ros::Publisher &coordPub,
    geometry_msgs::Transform &eyeToBase
) {
    // Covnert the input coords to eigen Vec3
    Eigen::Vector3d oldCoords;
    tf::vectorMsgToEigen(*msg, oldCoords);
    // Convert the tranform to an Eigen Isometry3d
    Eigen::Isometry3d eyeToBaseTF;
    tf::transformMsgToEigen(eyeToBase, eyeToBaseTF);
    // Actually perform the transform
    Eigen::Vector3d newCoords = eyeToBaseTF * oldCoords;
    // Convert back to geometry_msg
    geometry_msgs::Vector3 msgOut;
    tf::vectorEigenToMsg(newCoords, msgOut);
    // Publish
    coordPub.publish(msgOut);
}

int main(int argc, char** argv)
{
    // Ros Node Boilerplate
    ros::init(argc, argv, "coord_calc");
    ros::NodeHandle nh;

    // Initialise the TF buffers
    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener(tfBuffer);

    // Rate limiter for the main thread
    ros::Rate rate(1.0);

    // Transform representing eye to base
    geometry_msgs::Transform eyeToBase;

    // We should stick here until we at least get one successful transform
    while (!updateTf(eyeToBase, tfBuffer))
    {
        rate.sleep();
    }
    ROS_INFO("First Transformed received, beginning conversion.");

    // Now that we have one, we can start up the subscriber to translate the
    // coords
    ros::Publisher coordPub = nh.advertise<geometry_msgs::Vector3>("/pickup_pos", 10, false);
    ros::Subscriber coordSub = nh.subscribe<geometry_msgs::Vector3>("/relative_pose", 10, 
        boost::bind(coordSubCallback, _1, boost::ref(coordPub), boost::ref(eyeToBase))
    );

    // Start up a spinner to process the subcriber callbacks in the background
    ros::AsyncSpinner subSpinner(1);
    subSpinner.start();

    // Now, the main thread will just update the transform, while the subscriber
    // callback takes care of the coordinate translation. 
    while (nh.ok()) {
        updateTf(eyeToBase, tfBuffer);
        rate.sleep();
    }
    
    // Cleanup and shutdown
    subSpinner.stop();
    ros::shutdown();
    return 0;
}