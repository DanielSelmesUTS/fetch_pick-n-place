/**
 * @file pick-n-place.cpp
 * @author Daniel Selmes
 * @date 2021-05-17
 * @brief ROS C++ node which creates and executes a pick and place trajectory. 
 */

// INCLUDES
///////////

// Ros Runtime
#include <ros/ros.h>
// Various ROS geometry messages, the Pose is used to create the cartesian trajectories.
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Pose.h>
// Used for 3D geometry (rotations & translations)
#include <Eigen/Geometry>
// Used to convert the eigen Isometry3D objects to geometry_messages for moveit.
#include <eigen_conversions/eigen_msg.h>
// Exposes an interface to moveit for high level robot control operations
#include <moveit/move_group_interface/move_group_interface.h>
// Used to ensure execution of the pick and place process is stable
#include <mutex>
// Used to pass additional arguments to the ROS subscriber callback
#include <boost/bind.hpp>


// CONSTANTS AND CONFIGURATION
//////////////////////////////

//! Distance the gripper fingers will open to.  
static const double GRIPPER_OPEN_DIST = 0.05;
//! Distance the gripper fingers will close to. 
static const double GRIPPER_CLOSE_DIST = 0.025;

//! End effector rotation offset, used to ensure the gripper grabs from above.
const Eigen::AngleAxisd EE_ROT_OFFEST(M_PI/2.0, Eigen::Vector3d::UnitY());
//! End effector position offset, used to ensure the gripper uses the tips of the fingers to
const Eigen::Translation3d EE_POS_OFFSET(
   -0.175, 
    0.0, 
    0.025
);
//! End effector pickup translation offset, the position to approach from. 
const Eigen::Translation3d EE_APPROACH_OFFSET(
   -0.15, 
    0.0,
    0.0
);

//! Cartesian move tuning parameter, max distance between "linear" path points.
static const double CART_MOVE_EEF_STEP = 0.05; 
//! Cartesian move tuning parameter, distance in config allowed between points.
static const double CART_MOVE_JUMP_THRES = 100.0;


// HELPER FUNCTIONS
///////////////////

/**
 * @brief Opens the gripper using a moveit move group interface.
 * @param gripper_group Moveit move group interface to the gripper. 
 * @return Returns the moveit error code from the execution step. 
 */
moveit::planning_interface::MoveItErrorCode open_gripper(
    moveit::planning_interface::MoveGroupInterface &gripper_group
) {
    std::vector<double> gripperOpen = {
        GRIPPER_OPEN_DIST, GRIPPER_OPEN_DIST
    };
    gripper_group.setJointValueTarget(gripperOpen);
    return gripper_group.move();
}

/**
 * @brief Closes the gripper using a moveit move group interface. 
 * @param gripper_group Moveit move group interface to the gripper. 
 * @return Returns the moveit error code from the execution step. 
 */
moveit::planning_interface::MoveItErrorCode close_gripper(
    moveit::planning_interface::MoveGroupInterface &gripper_group
) {
    std::vector<double> gripperClosed = {
        GRIPPER_CLOSE_DIST, GRIPPER_CLOSE_DIST
    };
    gripper_group.setJointValueTarget(gripperClosed);
    return gripper_group.move();
}


// ROS TOPIC CALLBACKS
//////////////////////

/**
 * @brief Callback which executes a pick and place on the given position.
 *
 * This callback takes a single message with a coordinate, and attempts to do a
 * pick and place of an object located at that position. The position should be
 * given relative to the base_link frame of the robot, this can be found using
 * TF. The pick position is set to the position provided, and the place position
 * is mirrored along the XZ plane of the base_link frame. This usually means it
 * will move objects in front of the robot from the left side of the robot to
 * the right side, and vice versa. 
 *
 * First, a set of waypoints are generated. Note that this is only the geometry,
 * the actual robot waypoints are calculated on the fly in the execution stage.
 * This includes simple transformations to get the gripper facing the right
 * orientation, and a second waypoint above the pick and place positions. 
 *
 * Next, the path is executed. This involves joint-space moves to the approach
 * position, then cartesian moves for the pick and place, and finally a
 * joint-space move back to the starting position when the message was received.
 *
 * If something goes wrong in the execution, the robot will abandon the pick and
 * place operation. This can be for a variety of reasons, imost commonly,
 * controller faults (i.e. physical collisions) and path solving errors (i.e.
 * out of reach of the robot, self-colliding, etc.).
 *
 * @param msg The received coordinates to pick from.
 *
 * @param performingPnP A mutex which is locked when a pick and place operation
 * begins, and released when the current operation ends. Used to stop the
 * callbacks from fighting each other. 
 *
 * @param arm_group moveit move group interface for the robot arm. 
 *
 * @param gripper_group moveit move group interface for the end of arm gripper. 
 */
void pickPosCallback(
    const geometry_msgs::Vector3ConstPtr& msg,
    std::mutex &performingPnP,
    moveit::planning_interface::MoveGroupInterface &arm_group,
    moveit::planning_interface::MoveGroupInterface &gripper_group
) {
    ROS_INFO("Starting Pick and Place Operation (locking now)...");
    // PRE-EXECUTION
    // We lock this code to ensure that it can only be run once at a time, 
    // since we may receive another message while owrking on this one. 
    performingPnP.lock();

    // WAYPOINT GENERATION
    ROS_INFO("Generating Waypoints...");
    // Pull the pick position out of the axis
    Eigen::Isometry3d pickT;
    pickT =  Eigen::Translation3d(msg->x, msg->y, msg->z);
    // Rotate it so the robot picks the right way
    pickT *= EE_ROT_OFFEST;
    // Apply a small translation to the pickup point
    pickT *= EE_POS_OFFSET;
    // Create an approach waypoint above the pickup. 
    Eigen::Isometry3d approachPickT = pickT * EE_APPROACH_OFFSET;
    // We are going to make the place position the mirror of the pick position
    // along the centre of the base. This is essentially just the same as above
    // but we negate the Y component when making the target pose.
    Eigen::Isometry3d placeT;
    placeT =  Eigen::Translation3d(msg->x, -(msg->y), msg->z);
    placeT *= EE_ROT_OFFEST;
    placeT *= EE_POS_OFFSET;
    Eigen::Isometry3d approachPlaceT = placeT * EE_APPROACH_OFFSET;
    // Store the current waypoint so that we can return there once the pick and
    // place is done. 
    std::vector<double> startPose = arm_group.getCurrentJointValues();
    // For the cartesian paths, we need the pose versions of all the points.
    geometry_msgs::Pose pickP, approachPickP, placeP, approachPlaceP;
    tf::poseEigenToMsg(pickT, pickP);
    tf::poseEigenToMsg(approachPickT, approachPickP);
    tf::poseEigenToMsg(placeT, placeP);
    tf::poseEigenToMsg(approachPlaceT, approachPlaceP);

    // PATH EXECUTION
    ROS_INFO("Begin execution...");
    // Result of a move execution so that we can tell if it was successful or
    // not.
    moveit::planning_interface::MoveItErrorCode res;
    // Tracks how much of the path was planned. 
    double planProgress;
    // Move into the approach position for the pick.
    arm_group.setPoseTarget(approachPickT);
    res = arm_group.move();
    if (!res) {
        performingPnP.unlock();
        return;
    }
    // Ensure the gripper is open when we start
    if (!open_gripper(gripper_group)) {
        performingPnP.unlock();
        return;
    }
    // For the pick approach, we will use a cartesian move.
    std::vector<geometry_msgs::Pose> approachPickPs;
    approachPickPs.push_back(approachPickP);
    approachPickPs.push_back(pickP);
    moveit_msgs::RobotTrajectory approachPickTr;
    planProgress = arm_group.computeCartesianPath(
        approachPickPs, 
        CART_MOVE_EEF_STEP, CART_MOVE_JUMP_THRES, 
        approachPickTr
    );
    if (planProgress < 1.0) {
        performingPnP.unlock();
        return;
    }
    res = arm_group.execute(approachPickTr);
    if (!res) {
        performingPnP.unlock();
        return;
    }
    // Close the gripper. Here, path deviations are allowed, so we only abort if
    // something went wrong and it wasn't to do with the controller.
    //! TODO: Add checks for unexpected error conditions. For now, ROS_INFO the
    //! value for logging purposes.
    res = close_gripper(gripper_group);
    if (!res) {
        ROS_WARN("Gripper close execution failed with error code: %d", res.val);
    }
    // Now, we use a cartesian move to the do the place movement
    std::vector<geometry_msgs::Pose> pickNPlacePs;
    pickNPlacePs.push_back(pickP);
    pickNPlacePs.push_back(approachPickP);
    pickNPlacePs.push_back(approachPlaceP);
    pickNPlacePs.push_back(placeP);
    moveit_msgs::RobotTrajectory pickNPlaceTr;
    planProgress = arm_group.computeCartesianPath(
        pickNPlacePs, 
        CART_MOVE_EEF_STEP, CART_MOVE_JUMP_THRES, 
        pickNPlaceTr
    );
    if (planProgress < 1.0) {
        performingPnP.unlock();
        return;
    }
    res = arm_group.execute(pickNPlaceTr);
    if (!res) {
        performingPnP.unlock();
        return;
    }
    // Open the gripper
    if (!open_gripper(gripper_group)) {
        performingPnP.unlock();
        return;
    }
    // Finally, we do a cartesian retract back to the place approach
    std::vector<geometry_msgs::Pose> approachPlacePs;
    approachPlacePs.push_back(placeP);
    approachPlacePs.push_back(approachPlaceP);
    moveit_msgs::RobotTrajectory approachPlaceTr;
    planProgress = arm_group.computeCartesianPath(
        approachPlacePs, 
        CART_MOVE_EEF_STEP, CART_MOVE_JUMP_THRES, 
        approachPlaceTr
    );
    if (planProgress < 1.0) {
        performingPnP.unlock();
        return;
    }
    res = arm_group.execute(approachPlaceTr);
    if (!res) {
        performingPnP.unlock();
        return;
    }
    // Finished the pick and place, joints return to the starting pose. 
    ROS_INFO("Returning to initial pose...");
    arm_group.setJointValueTarget(startPose);
    res = arm_group.move();
    if (!res) {
        performingPnP.unlock();
        return;
    }

    // CLEANUP
    ROS_INFO("Pick and Place sequence complete!");
    // Release the lock now that we're done with the pick and place. 
    performingPnP.unlock();
}


// MAIN EXECUTION THREAD
////////////////////////

/**
 * @brief Starts up the node and prepares the subscriber.
 *
 * This is the main thread of execution for the ros node. The start is the usual
 * ROS boilerplate, and setup of some objects we need. Next, the move group
 * interface objects are created, which connects to moveit. This also involved
 * moving the arm and gripper into some sensible default positions. Finally, the
 * actual subscriber is started, and thus messages will now be handled by the
 * callback. At this point we are good to go and the lock is released. 
 *
 * This node uses a multi threaded spinner, but for now we only allocate one
 * thread to it. The reason for this is that moveit requires some sort of
 * spinner for its synchronous calls to work properly, since it has its own
 * subscribers to implement the move group interfaces. 
 *
 * Additionally, we start up a temporary asynchronous spinner for the same
 * reason when doing the initialisation movements. If something goes wrong
 * during these early movements, we're probably not in a good state to run, so
 * the node will crash in this case. 
 *
 * The usual CXX entry point arguments are used, although only by ROS for topic
 * remapping and other similar features. 
 */
int main(int argc, char** argv) {
    
    // Standard node init
    ros::init(argc, argv, "pick_n_place");
    ros::NodeHandle nh;
    ROS_INFO("Intializing Pick and Place System");
    
    // This is used to lock out the pick and place thread while we're working on
    // it.
    std::mutex performingPnP;
    // Aquire the pick and place thread lock so that we don't try to do that
    // while setting up.
    performingPnP.lock();

    // Spinner to ensure we can communicate with moveit properly
    ros::MultiThreadedSpinner spinner(2);

    // We need a spinner for this initial stage so that moveit is responsive
    ros::AsyncSpinner tmpSpinner(1);
    // Initialise the move group interface
    moveit::planning_interface::MoveGroupInterface arm_group("arm_with_torso");
    // Configure the arm group
    arm_group.setMaxAccelerationScalingFactor(0.5); // This should make it a little more spiffy
    arm_group.setMaxVelocityScalingFactor(0.5);
    tmpSpinner.start();
    // Move the robot to a good default position for pick and place
    std::vector<double> homePosJ = {
        0.0,
        M_PI/2.0,
       -M_PI/3.0, 
       -M_PI/2.0,
        M_PI/2.0,
        M_PI/6.0,
        M_PI/2.0,
        0.0
    };
    arm_group.setJointValueTarget(homePosJ);
    moveit::planning_interface::MoveItErrorCode res = arm_group.move();
    if (!res) {
        ROS_ERROR("Initialisation error: Failed to move to default pose.");
        ros::shutdown();
        exit(-1);
    }
    // Initialise the gripper controller
    moveit::planning_interface::MoveGroupInterface gripper_group("gripper");
    // Configure the gripper group
    gripper_group.setMaxAccelerationScalingFactor(1.0);
    gripper_group.setMaxVelocityScalingFactor(1.0);
    res = open_gripper(gripper_group);
    if (!res) {
        // It's probably a bad sign if opening the gripper fails, but just in
        // case it was a fluke we'll continue anyway. 
        ROS_WARN("Initialisation Error: Faield to open gripper to default size.");
    }
    tmpSpinner.stop();

    // Ready to receive pick and place requests, we can unlock now. 
    performingPnP.unlock();

    // Create the subscriber that will recieve the pick and place orders. Note
    // that I make the queue size 1. This is done so that we always get the
    // latest message from the topic. I know it's bad design, but I wanted this
    // to be simple, since it's essentially just running in a loop. It also
    // means there doesn't need to be any rate limiting on the other topics and
    // they can publish as often as necessary. 
    ros::Subscriber pickPosSub = nh.subscribe<geometry_msgs::Vector3>("/pickup_pos", 1, 
        boost::bind(&pickPosCallback, _1, boost::ref(performingPnP), boost::ref(arm_group), boost::ref(gripper_group))
    );

    ROS_INFO("Pick and place setup complete");

    // Spin the main thread to dispatch subscriber callbacks
    spinner.spin();

    ROS_INFO("Shutting down...");

    return 0;

}